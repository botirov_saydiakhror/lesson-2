# Assignment 2:
## Task list
Write a program that is divisible by 3 and 5 <br>
Write a program that checks whether given number odd or even.<br>
Write a program that get array as argument and sorts it by order // [1,2,3,4...]<br>
Write a program that return unique set of array:
input: =>  [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]

output: => [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]<br>
Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false  <br>
## License
[MIT](https://choosealicense.com/licenses/mit/)

<!-- This is my contribution - Madiyor Abdukhashimov -->