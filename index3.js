const { stdin, stdout } = process;

function prompt(question) {
  return new Promise((resolve, reject) => {
    stdin.resume();
    stdout.write(question);

    stdin.on('data', data => resolve(data.toString().trim()));
    stdin.on('error', err => reject(err));
  });
}


async function main() {
    var arr = [];
    var size = await prompt("enter the size of an array:");
  try {
      for (var i=0;i<size;i++) {
          arr.push(await prompt(i+1+"number input:"))
      }
    console.log(arr);
    console.log(arr.sort());
    stdin.pause();
  } catch(error) {
    console.log("There's an error!");
    console.log(error);
  }
  process.exit();
}

main();